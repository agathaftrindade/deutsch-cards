import argparse
import random

def parse_file(filename):

    lines = None
    with open(filename) as f:
        lines = f.readlines()

    cards = []
    current_section = None
    for line in lines[:]:
        line = line.strip()

        if not line: 
            continue

        if line.startswith('# '): 
            continue

        if line.startswith('## '):
            current_section = line[3:].strip()
            continue

        seps = [i for i, c in enumerate(line) if c == ':']
        term = line[ : seps[0]].strip()
        meaning = line[seps[0]+1 : ].strip()
        cards.append({
            'term': term,
            'meaning': meaning,
            'section': current_section
        })
    return cards

def create_csv(cards, outfile):

    def e(f):
        if ',' in f:
            return f'"{f}"'
        return f

    with open(outfile, 'w') as f:
        f.write(f"term,meaning,section\n")
        for card in cards:
            l = [
                e(card['term']),
                e(card['meaning']),
                e(card['section']).lower()
            ]
            f.write(",".join(l) + "\n")
            # f.write(f"{card['section']}")

def test_card(card):
    print(card['term'], end=': ')
    input('')
    print(card['meaning'])
    input('')

def train(cards, state=None):
    cards = cards.copy()
    if state is None:
        state = {}
    
    # selected = random.sample(cards, 25)
    random.shuffle(cards)
    for card in cards:
        test_card(card)


    
    


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('operation', choices=['show', 'train', 'csv'], type=str.lower)

    args = parser.parse_args()

    cards = parse_file('vocabulario.md')

    if args.operation == 'show':
        print(cards)
    
    if args.operation == 'train':
        train(cards)

    if args.operation == 'csv':
        csv_file = 'vocabulario.csv'
        create_csv(cards, csv_file)
        print(f'{csv_file} created')
